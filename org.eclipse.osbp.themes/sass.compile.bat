echo START com.vaadin.sass.SassCompiler

::base theme
echo - DEFAULT
echo - source=./scss/styles.scss
echo - target=./VAADIN/themes/osbp/styles.css
java -cp "./lib/*"  com.vaadin.sass.SassCompiler ./scss/styles.scss ./VAADIN/themes/osbp/styles.css

::theme flavor 4
echo - ENTRANCE
echo - source=./scss/styles-entrance.scss
echo - target=./VAADIN/themes/osbp-entrance/styles.css
java -cp "./lib/*"  com.vaadin.sass.SassCompiler ./scss/styles-entrance.scss ./VAADIN/themes/osbp-entrance/styles.css
echo DONE!

::theme flavor 3
echo - CXO
echo - source=./scss/styles-cxo.scss
echo - target=./VAADIN/themes/osbp-cxo/style.css
java -cp "./lib/*"  com.vaadin.sass.SassCompiler ./scss/styles-cxo.scss ./VAADIN/themes/osbp-cxo/styles.css
echo DONE!

::theme flavor 1
echo - STEEL
echo - source=./scss/styles-steel.scss
echo - target=./VAADIN/themes/osbp-steel/styles.css
java -cp "./lib/*"  com.vaadin.sass.SassCompiler ./scss/styles-steel.scss ./VAADIN/themes/osbp-steel/styles.css
echo DONE!

::theme flavor 2
echo - TEA
echo - source=./scss/styles-tea.scss
echo - target=./VAADIN/themes/osbp-tea/styles.css
java -cp "./lib/*"  com.vaadin.sass.SassCompiler ./scss/styles-tea.scss ./VAADIN/themes/osbp-tea/styles.css
echo DONE!
